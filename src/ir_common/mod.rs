use vec1::Vec1;

use crate::interner::{NameSymbol, StringSymbol};
use crate::Span;

#[cfg(feature = "serialize")]
use serde::Serialize;

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Identifier {
    pub span: Span,
    pub symbol: NameSymbol,
}
impl From<Identifier> for NameSymbol {
    fn from(item: Identifier) -> Self {
        item.symbol
    }
}
impl From<&Identifier> for NameSymbol {
    fn from(item: &Identifier) -> Self {
        item.symbol
    }
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct DottableId {
    pub span: Span,
    pub ids: Vec1<Identifier>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct StringConst {
    pub span: Span,
    pub symbol: StringSymbol,
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct NameConst {
    pub span: Span,
    pub symbol: NameSymbol,
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct IntConst {
    pub span: Span,
    pub val: u64,
    pub long: bool,
    pub unsigned: bool,
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct FloatConst {
    pub span: Span,
    pub val: f64,
    pub double: bool,
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[cfg_attr(feature = "serialize", serde(tag = "kind", content = "data"))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ConstKind {
    String(StringConst),
    Name(NameConst),
    Int(IntConst),
    Float(FloatConst),
    Bool(bool),
    Null,
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Const {
    pub span: Span,
    #[cfg_attr(feature = "serialize", serde(flatten))]
    pub kind: ConstKind,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct VersionInfo {
    pub major: u16,
    pub minor: u16,
    pub revision: u16,
}

impl VersionInfo {
    pub const fn new(major: u16, minor: u16, revision: u16) -> Self {
        Self {
            major,
            minor,
            revision,
        }
    }
}

pub(crate) fn parse_lump_version(s: &str) -> Option<VersionInfo> {
    fn split_once_inclusive(s: &str, p: impl Fn(char) -> bool) -> (&str, &str) {
        let mut splitter = s.splitn(2, p);
        let first = splitter.next().unwrap();
        let second = &s[first.len()..];
        (first, second)
    }

    let mut s = s;

    s = split_once_inclusive(s, |c| ![' ', '\n', '\t', '\r'].contains(&c)).1;
    let (s0, s1) = split_once_inclusive(s, |c| !c.is_ascii_digit());
    s = s1;
    let major = s0.parse::<u16>().ok()?;
    if !s.starts_with('.') {
        return None;
    }
    s = &s[1..];

    s = split_once_inclusive(s, |c| ![' ', '\n', '\t', '\r'].contains(&c)).1;
    let (s0, s1) = split_once_inclusive(s, |c| !c.is_ascii_digit());
    s = s1;
    let minor = s0.parse::<u16>().ok()?;

    let revision = if s.starts_with('.') {
        s = &s[1..];
        s = split_once_inclusive(s, |c| ![' ', '\n', '\t', '\r'].contains(&c)).1;
        let (s0, s1) = split_once_inclusive(s, |c| !c.is_ascii_digit());
        s = s1;
        s0.parse::<u16>().ok()?
    } else {
        0
    };

    if s.chars().next().is_some() {
        return None;
    }

    Some(VersionInfo::new(major, minor, revision))
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[cfg_attr(feature = "serialize", serde(tag = "kind", content = "data"))]
#[derive(Debug, Clone, PartialEq)]
pub enum ExpressionKind {
    Ident(Identifier),
    Const(Const),
    Binary {
        op: BinaryOp,
        exprs: Box<BinaryOpExprs>,
    },
    Prefix {
        op: PrefixOp,
        expr: Box<Expression>,
    },
    Postfix {
        op: PostfixOp,
        expr: Box<Expression>,
    },
    Ternary(Box<TernaryOpExprs>),
    ArrayIndex(Box<ArrayIndexExprs>),
    FunctionCall {
        lhs: Box<Expression>,
        exprs: Vec<FunctionCallArg>,
    },
    Vector2(Box<(Expression, Expression)>),
    Vector3(Box<(Expression, Expression, Expression)>),
    ClassCast(Identifier, Vec<FunctionCallArg>),
    Super,

    Unknown,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct Expression {
    pub span: Option<Span>,
    #[cfg_attr(feature = "serialize", serde(flatten))]
    pub kind: ExpressionKind,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BinaryOp {
    Add,
    Subtract,
    Times,
    Divide,
    Modulo,
    Raise,

    LeftShift,
    RightShift,
    UnsignedRightShift,

    CrossProd,
    DotProd,
    Concat,

    LessThan,
    LessThanEquals,
    GreaterThan,
    GreaterThanEquals,
    Equals,
    NotEquals,
    ApproxEquals,
    ThreeWayComp,

    LogicalAnd,
    BitwiseAnd,
    LogicalOr,
    BitwiseOr,
    BitwiseXor,

    Is,
    Scope,
    MemberAccess,

    Assign,
    PlusAssign,
    MinusAssign,
    TimesAssign,
    DivideAssign,
    ModuloAssign,
    LeftShiftAssign,
    RightShiftAssign,
    UnsignedRightShiftAssign,
    BitwiseOrAssign,
    BitwiseAndAssign,
    BitwiseXorAssign,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PrefixOp {
    Plus,
    Minus,
    Increment,
    Decrement,
    LogicalNot,
    BitwiseNot,
    SizeOf,
    AlignOf,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum PostfixOp {
    Increment,
    Decrement,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub enum LabeledStatement {
    Default,
    Case(Box<Expression>),
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct ExprList {
    pub span: Span,
    pub list: Vec1<Expression>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct ArraySizes {
    pub span: Span,
    pub list: Vec1<Option<Expression>>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct BinaryOpExprs {
    pub lhs: Expression,
    pub rhs: Expression,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct TernaryOpExprs {
    pub cond: Expression,
    pub if_true: Expression,
    pub if_false: Expression,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct ArrayIndexExprs {
    pub lhs: Expression,
    pub index: Expression,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[cfg_attr(feature = "serialize", serde(tag = "kind", content = "data"))]
#[derive(Debug, Clone, PartialEq)]
pub enum FunctionCallArgKind {
    Unnamed(Expression),
    Named(Identifier, Expression),
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct FunctionCallArg {
    pub span: Span,
    #[cfg_attr(feature = "serialize", serde(flatten))]
    pub kind: FunctionCallArgKind,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[cfg_attr(feature = "serialize", serde(tag = "kind", content = "data"))]
#[derive(Debug, Clone, PartialEq)]
pub enum IntTypeKind {
    SByte,
    Byte,
    Short,
    UShort,
    Int,
    UInt,
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct IntType {
    pub span: Span,
    #[cfg_attr(feature = "serialize", serde(flatten))]
    pub kind: IntTypeKind,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct EnumVariant {
    pub doc_comment: Option<StringSymbol>,
    pub span: Span,
    pub name: Identifier,
    pub init: Option<Expression>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct EnumDefinition {
    pub doc_comment: Option<StringSymbol>,
    pub span: Span,
    pub name: Identifier,
    pub enum_type: Option<IntType>,
    pub variants: Vec<EnumVariant>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct ConstDefinition {
    pub doc_comment: Option<StringSymbol>,
    pub span: Span,
    pub name: Identifier,
    pub expr: Expression,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct FlagDefinition {
    pub doc_comment: Option<StringSymbol>,
    pub span: Span,
    pub flag_name: Identifier,
    pub var_name: Identifier,
    pub shift: IntConst,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct PropertyDefinition {
    pub doc_comment: Option<StringSymbol>,
    pub span: Span,
    pub name: Identifier,
    pub vars: Vec1<Identifier>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[cfg_attr(feature = "serialize", serde(tag = "kind", content = "data"))]
#[derive(Debug, Clone, PartialEq)]
pub enum DefaultStatementKind {
    Property {
        prop: DottableId,
        vals: Option<ExprList>,
    },
    AddFlag(DottableId),
    RemoveFlag(DottableId),
}
#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct DefaultStatement {
    pub span: Span,
    #[cfg_attr(feature = "serialize", serde(flatten))]
    pub kind: DefaultStatementKind,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct DefaultDefinition {
    pub span: Span,
    pub statements: Vec<DefaultStatement>,
}

#[cfg_attr(feature = "serialize", derive(Serialize))]
#[derive(Debug, Clone, PartialEq)]
pub struct NonWhitespace {
    pub span: Span,
    pub symbol: NameSymbol,
}
